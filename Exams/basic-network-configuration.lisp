(in-package proctor)
;; This is a cert-proctor exam template, please follow the syntax or the exam configuration will be considered invalid.

;; *segments* mapped is 0 -> *A* through 23 -> *Z* currently.
(defparameter *segments* '(0 *A*
			   1 *B*
			   2 *C*
			   3 *D*))

;; *totalqs* is the total number of questions in your segment. Likewise 0 -> *A* through 23 -> *Z* is applicable here. (Note lisp starts counting at 0. So a segment with 6 questions will be 0-5 and should be listed as having 5 questions in *totalqs*
(defparameter *totalqs* '(0 4
			  1 4
			  2 6
			  3 5))

;; This is the blurb that appears at the beginning of the exam that explains exam rules, ie: the RHCSA mock exam lastest 2.5hrs, etc, etc.
(defvar *NOTICE* "Nothing yet..")

;; Each segment is a list that references a number to a question, starting at 0. The syntax is a simple Lisp S-expression. Mapping 0 -> *A* through 23 -> *Z* is applicable here.
;; /etc/network/interfaces
(defparameter *A* '(0 "Set eth0 to a static IP of 192.168.0.123, subnet of 255.255.255.0, and gateway of 192.168.0.1"
		    1 "Disable localhost communication."
		    2 "Configure an alternate configuration for eth0 that allows DHCP, while leaving the static configuration intact."
		    3 "Change the hostname of the system to NewNameNewServer."
		    4 "Create a configuration for eth1 that enables DHCP, and autostarts on plugin."))

;; /etc/resolv.conf
(defparameter *B* '(0 "Set a DNS server of 8.8.8.8, outside of the interface file."
		    1 "Set a DNS server of 208.67.222.222, outside of the interface file."
		    2 "Set a DNS server of 1.1.1.1, outside of the interface file"
		    3 "Set DNS to resolve from 8.8.8.8 through the interfaces file."
		    4 "Set DNS to resolve from 1.1.1.1, but additionally add a nameserver to eth0 to resolve from 8.8.8.8"))

;; finding information ip/ifconfig
(defparameter *C* '(0 "With the ip tool create a file in /answers containing the ip information of all nics on the system, named all-nic-info"
		    1 "With the ip tool create a file in /answers containing the ip information for eth0, named eth0-nic-info"
		    2 "With the ip tool create a file in /answers containing the ip information for lo, named lo-nic-info"
		    3 "With the ip tool create a file in /answers containing the ip route information, named ip-route-info"
		    4 "With the ifconfig tool create a file in /answers containing the ip information of all nics on the system, named ifc-all-nic-info"
		    5 "With the ifconfig tool create a file in /answers containing the ip information for lo, name ifc-lo-nic-info"
		    6 "With the ifconfig tool create a file in /answers containing the ip information for eth0, named ifc-eth0-nic-info"))

;; nmap/traceroute
(defparameter *D* '(0 "Produce a file in /answers called gateway-map containing a list of online systems on the same lan segment as the gateway."
		    1 "Produce a file in /answers called gateway-fast containing a list of commonly used ports that the gateway is using."
		    2 "Produce a file in /answers called gateway-all containing information about ports 1-65535 that the gateway is using."
		    3 "Create a file in /answers called route-to-google, that contains the node route to 8.8.8.8"
		    4 "Create a file in /answers called route-to-l1, that contains the node route to 1.1.1.1."

		    5 "Create a file in /answers called route-to-gateway, that contains the node route to the current system gateway."))
