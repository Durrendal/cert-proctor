(in-package proctor)
;; This is a cert-proctor exam template, please follow the syntax or the exam configuration will be considered invalid.

;; *segments* mapped is 0 -> *A* through 23 -> *Z* currently.
;; For exams where you desire to test someone against every single question, set *segments* to (0 *A*) and *totalqs* to (0 X) Configure *A* to contain all exam questions.
(defparameter *segments* '(0 *A*
			   1 *B*))

;; *totalqs* is the total number of questions in your segment. Likewise 0 -> *A* through 23 -> *Z* is applicable here. (Note lisp starts counting at 0. So a segment with 6 questions will be 0-5 and should be listed as having 5 questions in *totalqs*
(defparameter *totalqs* '(0 0
			  1 0
			  2 0))

;; This is the blurb that appears at the beginning of the exam that explains exam rules, ie: the RHCSA mock exam lastest 2.5hrs, etc, etc.
(defvar *NOTICE* "Nothing yet..")

;; Each segment is a list that references a number to a question, starting at 0. The syntax is a simple Lisp S-expression. Mapping 0 -> *A* through 23 -> *Z* is applicable here.
(defparameter *A* '(0 "Question 1"
		    1 "Question 2"))
