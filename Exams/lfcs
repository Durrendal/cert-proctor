(in-package proctor)
;; Exam sourced from here: http://mathematicbren.blogspot.com/2014/12/practice-test.html?m=1

;; *segments* mapped is 0 -> *A* through 23 -> *Z* currently.
(defparameter *segments* '(0 *A*))

;; *totalqs* is the total number of questions in your segment. Likewise 0 -> *A* through 23 -> *Z* is applicable here.
(defparameter *totalqs* '(0 30))

;; This is the blurb that appears at the beginning of the exam that explains exam rules, ie: the RHCSA mock exam lastest 2.5hrs, etc, etc.
(defvar *NOTICE* "Mock LFCS Exam, official exam time is 2 hours.")

;; Each segment is a list that references a number to a question, starting at 0. The syntax is a simple Lisp S-expression. Mapping 0 -> *A* through 23 -> *Z* is applicable here.
(defparameter *A* '(0 "You do not know the root password. Interrupt the startup and reset the root password to centos"
		    1 "Upon reboot, manually configure grub to boot into runlevel 3"
		    2 "Upon booting, set the machine to permanently boot into runlevel 3"
		    3 "Change the hostname to CENT7"
		    4 "Statically assign viable network settings for your machine: IP - DNS - FQDN - Gateway"
		    5 "Install the Apache Package"
		    6 "Change Apache's default directory to /web/html"
		    7 "Create a file named index.html containing the string 'It works!' and place it in /web/html"
		    8 "Make sure that anyone on your local subnet can access index.html, but no one else can."
		    9 "Create the following users: Bob, Tim, Ben"
		    10 "Create the following groups: Engineering, WebDev, Chemical."
		    11 "Set all user passwords to cent7"
		    12 "Set all accounts to expire in 1 years time"
		    13 "Set Tim's default home directory to /websites"
		    14 "Apply the same SELinux contexts and permissions to /websites that would be found on a users /home, such that Tim and only Tim has access to /websites"
		    15 "Give Ben complete acces to Sudo"
		    16 "Give Tim sudo access to iptables only"
		    17 "Create a directory named /Homework"
		    18 "Allow all users to read /Homework"
		    19 "Create a folder for each user named $user_homework in /Homework"
		    20 "Set RW access on $user_homework such that only $user has access to their given homework directory"
		    21 "Create a RAID 0 array using the two spare drives (5GB ea) on this machine: Size=2048, Label=RAID_0, mounted to /storage persistently"
		    22 "Create a RAID 1 array using LVM using the two spare drives (5GB ea) on this machine: Size=1024MB, Label=RAID_1, mounted to /storage2 persistently by UUID."
		    23 "Using the left over space on the 5GB drives, create a 1GB swap partition and add it to the existing swap pool: ensure swap is mounted at boot"
		    24 "Mount the NFS share on the KVM Hypervisor to /NFS_mount: ensure it is mounted persistently."
		    25 "Mount the CIFS share on the KVM Hypervisor to /CIFS_mount: ensure it is mounted persistently."
		    26 "Create a bash script named infinite_loop.sh that pints 'test' to /dev/null in an endless loop. Place the script in /root, ensure Bob has permissions to run it. Run the script as Bob, then as the root user locate the PID of the loop script, and forcefully kill it by PID."
		    27 "Create a backup of the /etc/yum.repos.d folder using tar or rsync; place your backup in /root"
		    28 "Create a local repo using a live dvd iso of Centos"
		    29 "Using your local repo, install the appropriate packages needed for virtualization."
		    30 "Download the text file found at 'http://www.gutenberg.org/cache/epub/844/pg844.txt': Manipulate the document such that every instance of the word Ernest is replaced with Earnest, Find line 3123 and put it at the bottom of the document, find line 2134 and put it at the top of the document, find line 2351 and put it at line 1532."))

(defvar provision "TRUE")

(defvar qemu-args "-o preallocation=metadata -f qcow2 /data/libvirt/images/centos.qcow2 25G")

(defvar virt-args "--virt-type kvm --name lfcsexam --ram 1024 --disk /data/libvirt/images/centos7.qcow2,format=qcow2 --network network=hostonly --os-type=linux --os-variant=rhel7.0 --location /materials/ISOs/CentOS7.iso")