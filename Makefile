LISP ?= sbcl
LISP_FLAGS ?= --no-userinit --non-interactive

QUICKLISP_URL = https://beta.quicklisp.org/quicklisp.lisp
DOWNLOAD_AGENT = curl
DOWNLOAD_AGENT_FLAGS = --output
QUICKLISP_DIR = quicklisp

all:
	$(MAKE) quicklisp
	$(MAKE) proctor
	$(MAKE) clean

quicklisp:
	mkdir -p $(QUICKLISP_DIR) ;\
	$(DOWNLOAD_AGENT) $(DOWNLOAD_AGENT_FLAGS) quicklisp.lisp $(QUICKLISP_URL)
	$(LISP) $(LISP_FLAGS) \
                --eval '(require "asdf")' \
                --load quicklisp.lisp \
                --eval '(quicklisp-quickstart:install :path "$(QUICKLISP_DIR)/")' \
                --eval '(uiop:quit)' \

	$(LISP) $(LISP_FLAGS) \
                --load ./quicklisp/setup.lisp \
                --eval '(require "asdf")' \
                --eval '(ql:update-dist "quicklisp" :prompt nil)' \
		--eval '(ql:quickload (list "asdf" "unix-opts" "uiop"))' \
                --eval '(uiop:quit)'

proctor:
	$(LISP) $(LISP_FLAGS) --eval '(require "asdf")' \
		--load ./src/proctor.asd \
		--load ./quicklisp/setup.lisp \
		--eval '(ql:quickload :proctor)' \
		--eval '(ql:quickload (list "unix-opts" "uiop"))' \
		--eval '(asdf:make :proctor)' \
		--eval '(quit)'
clean:
	rm quicklisp.lisp ;\
	rm -rf $(QUICKLISP_DIR) ;\
