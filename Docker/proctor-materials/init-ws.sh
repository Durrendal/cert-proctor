#!/bin/bash
# Author: Will Sinatra <wpsinatra@gmail.com>
# Initialize lighttpd webserver, with static configuration for Cert-Proctor

run_webserver(){
       
    lighttpd -D -f /etc/lighttpd/lighttpd.conf
}

run_webserver
