# Usage:

## Building:
From the root directory of the docker container issue

```
docker build -t proctor-materials .
```

## Setup:
Create a directory containing your static exam examples, kickstart files, cert-proctor binaries, and any additional resources, such as offline package repos.

Enabled selinux contenxt on your hypervisor to allow the container to access your materials directory.

```
chcon -Rt svirt_sandbox_file_t /path/to/materials/
```

The container provides port 8080 as the webserver port, as such it will need to be added to your firewall. If you're running a RHEL based server as your hypervisor this can be accomplished trivially with the following command. The zone can of course be changed, I personally suggest creating a specific zone and vlan for your ephemeral test servers, and configuring these changes such that only the test systems have access to the materials.

```
firewall-cmd --zone=public --permanent --add-port=8080/tcp
firewal-cmd --zone=public --runtime --add-port=8080/tcp
```

If you wish to setup an offline package repo from an installation media it can be done with the following command. This is particularly useful if you wish to spin up RHEL7.7 vms for mock exams, but will not be licensing them during the course of the mock exam.

```
mount -o loop,ro -t iso9660 rhel-server-7.7-x86_64.dvd.iso new-dir/
```

## Running:
Running the container is simple, you will likely want to use the provide systemd script to start and stop it with

```
systemctl start/stop/restart/enable/disable proctor-materials
```

alternately the container can be run manually with

```
docker run --net=host -v /path/to/materials/:/tmp/site proctor-materials
```

