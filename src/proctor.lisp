;; Author: Will Sinatra <wpsinatra@gmail.com>
;; License: GPLv3
;; Generate, Proctor, and Grade hands on Linux exams
(uiop:define-package #:proctor
    (:use :cl :uiop)
  (:export :main))

(in-package proctor)

;;===== Variables =====
(defvar exam-path "~/exam.txt")
(defvar segments NIL)

;;===== Helper Functions =====
(defmacro suppress-stdout (&body body)
  `(with-output-to-string (*standard-output*
			   (make-array '(0)
				       :element-type 'base-char
				       :fill-pointer 0 :adjustable t))
			  ,@body))

;;===== Exam Functions =====
(defun gather-segments ()
  (format t "Total Segments:~%")
  (setf segments
	(princ (round (- (/ (length *segments*) 2.00) 1))))
  (terpri))

(defun q-params (qpos qseg)
  (progn
    (let
	((len (getf *totalqs* qpos)))
      (format NIL "~a" (getf qseg (random len))))))

(defun generate-test ()
  (with-open-file (stream exam-path :direction :output :if-exists :supersede)
    (format stream "~a~%~%" *NOTICE*)
    (if (eql segments 0)
	(progn
	  (let
	      ((len (getf *totalqs* 0)))
	    (loop for i from 0 to len
	       do
		 (format stream "~a~%" (concatenate 'string (write-to-string i) ") " (getf *A* i))))))
	(loop for i from 0 to segments
	   do
	     (progn
	       (cond
		 ((equal i 0)
		  (format stream "1) ~a~%" (q-params 0 *A*))
		  (format stream "2) ~a~%" (q-params 0 *A*)))
		 ((equal i 1)
		  (format stream "3) ~a~%" (q-params 1 *B*))
		  (format stream "4) ~a~%" (q-params 1 *B*)))
		 ((equal i 2)
		  (format stream "5) ~a~%" (q-params 2 *C*))
		  (format stream "6) ~a~%" (q-params 2 *C*)))
		 ((equal i 3)
		  (format stream "7) ~a~%" (q-params 3 *D*))
		  (format stream "8) ~a~%" (q-params 3 *D*)))
		 ((equal i 4)
		  (format stream "9) ~a~%" (q-params 4 *E*))
		  (format stream "10) ~a~%" (q-params 4 *E*)))
		 ((equal i 5)
		  (format stream "11) ~a~%" (q-params 5 *F*))
		  (format stream "12) ~a~%" (q-params 5 *F*)))
		 ((equal i 6)
		  (format stream "13) ~a~%" (q-params 6 *G*))
		  (format stream "14) ~a~%" (q-params 6 *G*)))
		 ((equal i 7)
		  (format stream "15) ~a~%" (q-params 7 *H*))
		  (format stream "16) ~a~%" (q-params 7 *H*)))
		 ((equal i 8)
		  (format stream "17) ~a~%" (q-params 8 *I*))
		  (format stream "18) ~a~%" (q-params 8 *I*)))
		 ((equal i 9)
		  (format stream "19) ~a~%" (q-params 9 *J*))
		  (format stream "20) ~a~%" (q-params 9 *J*)))
		 ((equal i 10)
		  (format stream "21) ~a~%" (q-params 10 *K*))
		  (format stream "22) ~a~%" (q-params 10 *K*)))))))))
;; Can easily expand to cover 24 total segments, additional after that if we do A1 A2 A3 so on so forth.

(defun grade-exam ()
  (with-open-file (stream "results.txt" :direction :output :if-exists :supersede)
		  (format stream "We currently don't have grading logic, thanks for using in-dev software!")))

(defun monitor-exam ()
  (loop
   (if (equal (probe-file ".grademe") NIL)
       (sleep 90)
     (progn
       (grade-exam)
       (sb-ext:quit)))))

(defun prepare-exam (exam)
  (load exam)
  (gather-segments)
  (generate-test)
  (format t "The exam has been generated, and can be found at ~a~%" exam-path)
  (if (eql provision "TRUE")
      (format t "No Exam Server Defined~%")
      (progn
	(format t "Preparing Exam VM~%")
	(uiop:run-program (concatenate 'string "qemu-img create " qemu-args) :output t)
	(uiop:run-program (concatenate 'string "virt-install " virt-args) :output t)
	(format t "Exam VM provisioned~%")))
  (sb-ext:quit))

(opts:define-opts
    (:name :help
	   :description "Helpful help messages"
	   :short #\h)
    (:name :exam
	   :description "Path to exam file"
	   :short #\e
	   :long "exam"
	   :arg-parser #'identity))

(defun unknown-option (condition)
  "If option is unknown"
  (format t "[WARN]  ~s is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  "When option is called, do"
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun main ()
  "Launch proctor with arg'd exam"
  (multiple-value-bind (options)
      (handler-case
	  (handler-bind ((opts:unknown-option #'unknown-option))
	    (opts:get-opts))
	(opts:missing-arg (condition)
	  (format t "[ERR]  Option ~s needs an argument~%"
		  (opts:option condition)))
	(opts:arg-parser-failed (condition)
	  (format t "[ERR]  Cannot parse ~s as argument of ~s~%"
		  (opts:raw-arg condition)
		  (opts:option condition)))
	(opts:missing-required-option (con)
	  (format t "[ERR]  ~a~%" con)
	  (opts:exit 1)))
    (when-option (options :help)
		 (opts:describe
		  :prefix "Proctor -- Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
		  :usage-of "proctor"))
    (when-option (options :exam)
		 (prepare-exam (getf options :exam)))))
