(defpackage #:proctor
  (:use :cl :asdf))

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))

(asdf:defsystem #:proctor
		:description "Certification Proctor"
		:author "Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>"
		:license "GPLv3"
		#+asdf-unicode :encoding #+asdf-unicode :utf-8
		:serial t
		:build-operation "program-op"
		:build-pathname "proctor"
		:entry-point "proctor:main"
		:depends-on (#:unix-opts
			     #:uiop)
		:components ((:file "proctor")))
