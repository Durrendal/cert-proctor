#!/bin/bash

run_the_gambit() {
    echo "Running ALL the playbooks!"

    echo "Configuring Docker"
    ansible-playbook configure-podman.yml

    echo "Configuring Libvirtd"
    ansible-playbook configure-libvirtd.yml

    echo "Configure users"
    ansible-playbook configure-users.yml

    echo "Configure Openssh & F2B"
    ansible-playbook configure-openssh.yml

    echo "Pull ISOs"
    ansible-playbook get-isos.yml

    echo "Configure Offline Repos"
    ansible-playbook make-repos.yml

    echo "Configure Selinux"
    ansible-playbook configure-selinux.yml

    echo "Restart SSHD"
    ansible-playbook restart-ssh.yml
}

run_the_gambit
