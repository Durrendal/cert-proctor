# Cert-Proctor

Cert-Proctor is an automated test generation, monitoring, and grading service meant to be used to provide robust RHCSA style lab exams for various certifications. Cert-Proctor is a work in progress, and currently only has bare bones functionality, but is built with the intent and purpose of providing quality preperation. Any suggestions on supported certs/labs/topics are greatly appreciated.

## Usage:

```
Proctor -- Author: Will Sinatra <will.sinatra@klockwork.net> <wpsinatra@gmail.com>

Usage: proctor [-h] [-e|--exam ARG]

Available options:
  -h                       Helpful help messages
  -e, --exam ARG           Path to exam file
```

To generate an LFCS exam for example you would do:

```
proctor -e lfcs
```

## Building:

Simply run make in the root directory of this repo. The proctor binary will be found in ./src

## Setting up the Hypvisor:

The hypervisor can be provisioned with the following steps:

Place hypervisor.cfg on a webserver, or NFS share
Boot your target system with a CentOS8 ISO

From the ISO splash screen press ESC, then enter linux inst.ks=hypervisor.cfg
(I typically use a webserver on a docker container, so: linux inst.ks=http://x.x.x.x:8080/Kickstart/hypervisor.cfg)

Allow the kickstart to run, reboot when finished.

After the system has been rebooted, run the gambit of ansible playbooks found in the ansible directory to:

Configure libvirtd
Configure Users
Configure Fail2Ban & Openssh
Pull ISOs for Alpine 3.11, CentOS 7, CentOS 8, Debian 10, Ubuntu 18.04, Ubuntu 19.10
Make repos from pulled ISO files

NOTE: You will need to add some basic configurations to the ansible playbooks, such as user names, and place authorized_keys into the ssh_confs/$username directories!

## TODO:

- [x] Simple Exam generation
- [] Grading Utility
- [x] Exam kickstart
- [x] Hypervisor kickstart
- [] Test Timer
- [x] Hypervisor package repo container
- [] XK0-004 Lin+ Labs
- [] More complex exam generation
- [] RHCE Labs
- [x] Makefile

## License:

This software is licensed under the GPLv3 license, further information can be found in the license file.

## Contact:

For questions, or suggestions, I can be reached at wpsinatra@gmail.com